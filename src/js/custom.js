  /*scroll down*/
        /*$(document).ready(function() {
            $('.go-down').click(function(){
                $('html, body').animate({scrollTop:$(document).height()}, 1500);
                     return false;
                });       
        });*/
         

        /*scroll up*/
         
        $('.go-up,.go-top').click(function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop: 0},1500);
        });

         /*menu*/
         function openNav() {
             document.getElementById("myNav").style.display = "block";
         }         
         function closeNav() {
             document.getElementById("myNav").style.display = "none";
         }
                 
    /*scroll reveal*/

        /* ScrollReveal().reveal('.scroll_reveal', { duration: 2000 }, 50);*/
        /* window.sr = ScrollReveal();
         sr.reveal('.scroll_reveal', { duration: 2000 }, 50);*/



       /*cookies*/
        $(document).ready(function(){   
          setTimeout(function () {
            $("#cookieConsent").fadeIn(200);}, 2000);
            $("#closeCookieConsent").click(function() {
            $("#cookieConsent").fadeOut(200);
          }); 
        }); 
  
        /* ----sticky menu----*/
         
        $(function() {
         $(window).scroll(function() {    
          if($(this).scrollTop()>50){
             $(".scroll_menu").addClass("scrolled");
           }         
         else {
            $(".scroll_menu").removeClass("scrolled");
          }
         });         
        });
                  
        $(function(){
         $(window).scroll(function(){
              if($(this).scrollTop()>5){
                  $(".scroll_menu img").attr("src","image/finca-biniagual-blanco.png");
              }
              else{
                  $(".scroll_menu img").attr("src","image/finca-biniagual-blanco.svg");
              }
          });
        });

          /* ----inner page sticky menu----*/
        $(function() {
            $(window).scroll(function() {    
                if($(this).scrollTop()>50){
                   $(".scroll_inner_menu").addClass("scrolled");        
                 } 
               else {
                  $(".scroll_inner_menu").removeClass("scrolled");
               }
            });     
        });
                      
        $(function(){
          $(window).scroll(function(){
              if($(this).scrollTop()>5){
                  $(".scroll_inner_menu img").attr("src","image/finca-biniagual-blanco.png");
                  $(".inner_openmenu").css("color", "#fdf7f0");
               }
              else{
                  $(".scroll_inner_menu img").attr("src","image/finca-biniagual-negro.svg");
                  $(".inner_openmenu").css("color", "#2c2c2c");
              }
          });
        });

        $(document).ready(function(){
           $(".filter-button").click(function(){
           var value = $(this).attr('data-filter');
           
              if(value == "all")
                {
                  $('.filter').show('1000');
                }
                else
                {
                    $(".filter").not('.'+value).hide('3000');
                    $('.filter').filter('.'+value).show('3000');
                }
              });
               if ($(".filter-button").removeClass("active")) {
                   $(this).removeClass("active");
                 }
                   $(this).addClass("active");   
        });

        $(function() {
            $('#contactform').validate({
                rules: {
                  name: {
                    required: true
                  },
                  email: {
                    required: true,
                    email: true
                  },
                  contact: {
                     minlength: 10,
                     maxlength:12,
                     required: true,
                     number: true
                  }
                },
                messages: {
                  name: "necesitas completar tu nombre",
                  email: "Necesitas rellenar con una dirección de correo electrónico válida el campo para enviar el formulario.",
                  contact:"necesitas completar tu numero",
                },
                errorElement : 'div',
                errorLabelContainer: '.errors'
              });
        });


       $(document).ready(function(){
        $("a").on('click', function(event) {
          if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
              scrollTop: $(hash).offset().top
            }, 800, function(){
              window.location.hash = hash;
            });
          } 
  });
});



