import React, { Component } from 'react';
import { Map, GoogleApiWrapper, InfoWindow, Marker } from 'google-maps-react';



const mapStyles = {
  position: 'absolute'
  // width: '80%',
  // height: '50%'
};

const placeList=[
  {
    "id": 1,
    "name": "Connaught Place, New Delhi",
    "lat": "28.6315",
    "lng": "77.2167"
  },
  {
    "id": 2,
    "name": "Karol Bagh Metro Station, Delhi",
    "lat": "28.6440",
    "lng": "77.1884"
  },
  {
    "id": 3,
    "name": "Mayur Vihar, Phase 1, Delhi",
    "lat": "28.6066",
    "lng": "77.2937"
  },
  {
    "id": 4,
    "name": "Connaught Place, New Delhi",
    "lat": "28.6315",
    "lng": "77.2167"
  },
  {
    "id": 5,
    "name": "Karol Bagh Metro Station, Delhi",
    "lat": "28.6440",
    "lng": "77.1884"
  },
  {
    "id": 6,
    "name": "Mayur Vihar, Phase 1, Delhi",
    "lat": "28.6066",
    "lng": "77.2937"
  }, {
    "id": 7,
    "name": "Connaught Place, New Delhi",
    "lat": "28.6315",
    "lng": "77.2167"
  },
  {
    "id": 8,
    "name": "Karol Bagh Metro Station, Delhi",
    "lat": "28.6440",
    "lng": "77.1884"
  },
  {
    "id": 9,
    "name": "Mayur Vihar, Phase 1, Delhi",
    "lat": "28.6066",
    "lng": "77.2937"
  }
]
// const arrylst=placeList.map((lsit,i)=>{
//   return <Place id={placeList[i].id} name={placeList[i].name} />
//   })
export class MapContainer extends Component {


  state = {
    showingInfoWindow: false,  //Hides or the shows the infoWindow
    activeMarker: {},          //Shows the active marker upon click
    selectedPlace: {}          //Shows the infoWindow to the selected place upon a marker
  };
  onMarkerClick = (props, marker, e) =>
  this.setState({
    selectedPlace: props,
    activeMarker: marker,
    showingInfoWindow: true
  });
  onClose = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  


  render() { 
    return (
   <div className="row">
    <div>
      <Map
        google={this.props.google}
        zoom={14}
        style={mapStyles}
        initialCenter={{
         lat: 28.6315,
         lng: 77.2167
        }}
      >
     { //curly brace here lets you write javscript in JSX
        placeList.map(place =>
            <Marker
            
              key={place.id}
              title={place.name}
              onClick={this.onMarkerClick}
              name={place.name}
              position={{ lat: place.lat, lng: place.lng }}
             
            />
        )
      } 
      
     <InfoWindow
      marker={this.state.activeMarker}
      visible={this.state.showingInfoWindow}>
        <div>
          <h1>{this.state.selectedPlace.name}</h1>
        </div>
     </InfoWindow>
    </Map>
    </div>
   
    </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyC3GMVxK0quFRRunAW0-_Ciyjnw4VWDEtc'
})(MapContainer);