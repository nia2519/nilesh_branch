import React, { Component } from 'react';


class Place extends Component{
    render(props){
        return <div className="col-md-4 full_box">
        <div className="slider_box">
           <i className="fa fa-map-marker"></i>
           <span>{this.props.name}
           </span>
        </div>
        <div className="full_div">
           <div className="left_div"><span>Lat : {this.props.lat}</span></div>
           <div className="right_div"><span>Lng : {this.props.lng}</span></div>
        </div>
     </div>
    
    }
   

}
export default Place