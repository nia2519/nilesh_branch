import React, { Component } from 'react';
import App from './App';
import Place from './Place';

import {Bar} from 'react-chartjs-2';
import axios from 'axios';
import './css/style.css';

const placeList=[
   {
     "id": 1,
     "name": "Connaught Place, New Delhi",
     "lat": "28.6315",
     "lng": "77.2167"
   },
   {
     "id": 2,
     "name": "Karol Bagh Metro Station, Delhi",
     "lat": "28.6440",
     "lng": "77.1884"
   },
   {
     "id": 3,
     "name": "Mayur Vihar, Phase 1, Delhi",
     "lat": "28.6066",
     "lng": "77.2937"
   },
   {
     "id": 4,
     "name": "Connaught Place, New Delhi",
     "lat": "28.6315",
     "lng": "77.2167"
   },
   {
     "id": 5,
     "name": "Karol Bagh Metro Station, Delhi",
     "lat": "28.6440",
     "lng": "77.1884"
   },
   {
     "id": 6,
     "name": "Mayur Vihar, Phase 1, Delhi",
     "lat": "28.6066",
     "lng": "77.2937"
   }, {
     "id": 7,
     "name": "Connaught Place, New Delhi",
     "lat": "28.6315",
     "lng": "77.2167"
   },
   {
     "id": 8,
     "name": "Karol Bagh Metro Station, Delhi",
     "lat": "28.6440",
     "lng": "77.1884"
   },
   {
     "id": 9,
     "name": "Mayur Vihar, Phase 1, Delhi",
     "lat": "28.6066",
     "lng": "77.2937"
   }
 ]
 const arrylst=placeList.map((lsit,i)=>{ 
 if(i<3)
   return <Place id={placeList[i].id} name={placeList[i].name} lat={placeList[i].lat} lng={placeList[i].lng} />
     
})

const arrylist2=placeList.map((lst,i)=>{
   if(i>=3 && i<6)
   return <Place id={placeList[i].id} name={placeList[i].name} lat={placeList[i].lat} lng={placeList[i].lng} />
});
const arrylst3=placeList.map((ls,i)=>{
   if(i>=6 && i<9)
   return <Place id={placeList[i].id} name={placeList[i].name} lat={placeList[i].lat} lng={placeList[i].lng} />
});



const weatherlist=[];

export class Map extends Component{
 
   constructor(props) {  
      super(props);
       this.state = { wData: [] }; 
      
      }  
     
   componentDidMount() {
     
      axios.get('http://api.worldweatheronline.com/premium/v1/weather.ashx?key=4a8e28a74c4747a586652956201006&q=28.6315,77.2167&num_of_days=7&tp=3&format=json')
      .then(response => {
        //console.log(response.data.data.weather);
       // Data= response.data.data.weather;
       // console.log(Data);
       const date=[];
       response.data.data.weather.map((ls,i)=> {
         date.push({"date": response.data.data.weather[i].date , "MaxTemp":response.data.data.weather[i].maxtempC,"MinTemp":response.data.data.weather[i].mintempC});
        
       });
       this.setState({
          wData:date
          
       })
       const day=[];
       const temp=[];
       response.data.data.weather.map((ls,i)=> {
         day.push(response.data.data.weather[i].date);
         temp.push(response.data.data.weather[i].maxtempC);
        
        
       });

       this.setState({
         chartdata:{
   labels:day,
   datasets:[
   {
     label:"tem data",
     data:temp,
     backgroundColor:[
       "#3cb371",
       "#0000FF",
       "#9966FF", 
       "#4C4CFF",
       "#00FFFF", 
       "#f990a7",  
       "#aad2ed"
     ]
   
   }]
   
   }
       });
      //   response.data.data.weather.forEach(element => {
           
      //   });((ls,i)=>{
      //    wdata.push(response.data.data.weather[i].date);
      //    return <Weatherdata date={response.data.data.weather[i].date} />
      // });
      console.log(this.state.wData);
      // weatherlist=this.state.wData.date.map((ls,i)=>{
      //    return <Weatherdata date={this.state.wData} />
      // })
    
      })
      .catch(error => {
        console.log(error);
      });
    }

 getweather(object) {
   console.log(object.lat);
   axios.get('http://api.worldweatheronline.com/premium/v1/weather.ashx?key=4a8e28a74c4747a586652956201006&q='+object.lat+','+object.lng+'&num_of_days=7&tp=3&format=json')
   .then(response => {
     //console.log(response.data.data.weather);
    // Data= response.data.data.weather;
    // console.log(Data);
    const date=[];
    response.data.data.weather.map((ls,i)=> {
      date.push({"date": response.data.data.weather[i].date , "MaxTemp":response.data.data.weather[i].maxtempC,"MinTemp":response.data.data.weather[i].mintempC});
     
    });
    this.setState({
       wData:date
       
    })

    const day=[];
    const temp=[];
    response.data.data.weather.map((ls,i)=> {
      day.push(response.data.data.weather[i].date);
      temp.push(response.data.data.weather[i].maxtempC);
     
     
    });
    
    this.setState({
      chartdata:{
labels:day,
datasets:[
{
  label:"tem data",
  data:temp,
  backgroundColor:[
    "#3cb371",
    "#0000FF",
    "#9966FF", 
    "#4C4CFF",
    "#00FFFF", 
    "#f990a7",  
    "#aad2ed"
  ]

}]

}
    });

   //   response.data.data.weather.forEach(element => {
        
   //   });((ls,i)=>{
   //    wdata.push(response.data.data.weather[i].date);
   //    return <Weatherdata date={response.data.data.weather[i].date} />
   // });
  // console.log(this.state.wData);
   // weatherlist=this.state.wData.date.map((ls,i)=>{
   //    return <Weatherdata date={this.state.wData} />
   // })
 
   })
   .catch(error => {
     console.log(error);
   });

  }  
    
    render(){

      const { wData } = this.state;
        return(
            <div className="map_section">   
            <div className="container-fluid">
              <div className="com-md-12 main_div">
                 <div className="row">
                    <div className="col-md-1 col-lg-1 col-2 left_side">
                       <ul className="nav nav-tabs" id="myTab" role="tablist">
                          <li className="nav-item">
                             <a className="nav-link active" data-toggle="tab" href="#home" role="tab" aria-controls="home"><span className="fa fa-map-marker"></span></a>
                          </li>
                          <li className="nav-item">
                             <a className="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"><span className="fa fa-snowflake-o"></span> 	 </a>
                          </li>
                       </ul>
                    </div>
                    <div className="col-md-11 col-lg-11 col-10 right_side">
                       <div className="tab-content custom_tab">
                          <div className="tab-pane active" id="home" role="tabpanel">
                              <div className="map_img">
                                     <App />
                             </div>
                            <div id="demo" className="carousel slide custom_slider" data-ride="carousel">
                               
                                <ul className="carousel-indicators">
                                   <li data-target="#demo" data-slide-to="0" className="active"></li>
                                   <li data-target="#demo" data-slide-to="1"></li>
                                   <li data-target="#demo" data-slide-to="2"></li>
                                   
                                </ul>
                               
                                <div className="carousel-inner">
                                <div className="carousel-item active">
                                      <div className="com-md-12">
                                        
                                         <div className="row">
                                         {arrylst}
                                            
                                         </div>
                                      </div>
                                   </div>
                                
                                   <div className="carousel-item">
                                      <div className="com-md-12">
                                        
                                         <div className="row">
                                             
                                            {arrylist2}
                                         </div>
                                      </div>
                                   </div>
                                   <div className="carousel-item">
                                      <div className="com-md-12">
                                        
                                         <div className="row">
                                             
                                            {arrylst3}
                                         </div>
                                      </div>
                                   </div>
                                    
                                  
                                   <a className="carousel-control-prev" href="#demo" data-slide="prev">
                                   <span className="fa fa-chevron-left"></span>
                                   </a>
                                   <a className="carousel-control-next" href="#demo" data-slide="next">
                                   <span className="fa fa-chevron-right"></span>
                                   </a>
                                </div>
                             </div>
                             
                          </div>
                          <div className="tab-pane second_tab" id="profile" role="tabpanel">
                          <div className="col-md-12">
                          <div className="row">
                                      <div className="col-md-5 custom_slider">
                                         <div className="row slider_height">

                                         {placeList.map(data=>(
                                            <div className="col-md-12 full_box">
                                           
                                            <div className="slider_box">
                                               <i className="fa fa-map-marker"></i>
                                               <span> <a onClick={this.getweather.bind(this, data)}> {data.name} </a> 
                                               </span>
                                            </div>
                                            <div className="full_div">
                                               <div className="left_div"><span>Lat :{data.lat}</span></div>
                                               <div className="right_div"><span>Lng :{data.lng}</span></div>
                                            </div>
                                         </div>
                                         )

                                         )}

                                       
                                           
        
        
                                         </div>
                                        
                                      </div>
        
                                      <div className="col-md-5 custom_slider" style={{overflow: "initial"}}>
                                         <div className="row slider_height" style={{height: "600px"}}>
 <div className="col-md-12">
                                                  <div className="col-md-5"><div className="slider_box"><span>Connaught place, New Delhi</span></div></div>
                                                  <div className="col-md-5"><div className="slider_box"><span>June 11-16 2020</span></div></div>
                                                  <hr/>  </div> 

                                                  {wData.map(Data => (
        <div className="col-md-12 full_box" >
            <div className="slider_box">
              
              <span>  {Data.date}
              </span>
             
             <span style={{padding:"100px"}}>{Data.MaxTemp}</span>
                                                  <span>{Data.MinTemp}</span>
         
           </div>
       
        </div>
      ))
    }
                                                 
                                                
        
                                         </div>
                                        
                                      </div>
        </div>
                              
                                        
                                  
                                  </div> 
                                  <div>
                                  <Bar data={this.state.chartdata}></Bar>



                                        </div>   
                                 
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
              </div>
              </div>
      
            
        )
    }
}
export default Map